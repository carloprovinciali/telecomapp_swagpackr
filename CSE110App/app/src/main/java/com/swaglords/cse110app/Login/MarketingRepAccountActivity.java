package com.swaglords.cse110app.Login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.swaglords.cse110app.R;

/**
 * Created by Tyler on 2/24/2015.
 */
public class MarketingRepAccountActivity extends ActionBarActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Parse.initialize(this, "ktmIrppN0aJb1J2kI8o2jRiAJJPSEAnYSxQZp9iF", "wSJeYJfZWko5zq5Cl2ofYGrWFIKmJSjuhVYAyo1n");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marketing_rep_account);

        //Set homeLogin to link to SignUpOrLoginActivity
        findViewById(R.id.cable_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =
                        new Intent(MarketingRepAccountActivity.this, MarketingRepAccountCableActivity.class);
                startActivity(intent);
            }
        });
        //Set homeSignUp to link to SignUpActivity
        findViewById(R.id.internet_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =
                        new Intent(MarketingRepAccountActivity.this, MarketingRepAccountInternetActivity.class);
                startActivity(intent);
            }
        });
        //Set homeLogin to link to SignUpOrLoginActivity
        findViewById(R.id.phone_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =
                        new Intent(MarketingRepAccountActivity.this, MarketingRepAccountPhoneActivity.class);
                startActivity(intent);
            }
        });
        //Set homeSignUp to link to SignUpActivity
        findViewById(R.id.existing_packages_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =
                        new Intent(MarketingRepAccountActivity.this, MarketingRepAccountExistingPackagesActivity.class);
                startActivity(intent);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
