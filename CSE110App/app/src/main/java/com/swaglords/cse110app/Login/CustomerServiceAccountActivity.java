package com.swaglords.cse110app.Login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.swaglords.cse110app.Account.CustomerAccount;
import com.swaglords.cse110app.R;

import java.util.List;

public class CustomerServiceAccountActivity extends ActionBarActivity
{
    private EditText customerLookup;
    private String username;
    private CustomerAccount customer;


	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_customer_service_account);

        customerLookup = (EditText) findViewById(R.id.searchUserText);
		customer = new CustomerAccount();

        findViewById(R.id.searchUserButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
				username = customerLookup.getText().toString().toLowerCase();
                ParseQuery<ParseUser> query = ParseUser.getQuery();
				query.whereEqualTo("username", username);
                query.findInBackground(new FindCallback<ParseUser>()
				{
					@Override
					public void done(List<ParseUser> parseUsers, ParseException e)
					{
						if (e == null)
						{
							Intent intent = new Intent(CustomerServiceAccountActivity.this, CustomerAccountActivity.class);

							ParseUser parseUser = parseUsers.get(0);

							// account.setAccountType(Account.AccountType.CUSTOMER_ACCOUNT);
							customer.setUsername(parseUser.getUsername());
							customer.setEmail((String) parseUser.getEmail());
							customer.setFirstName((String) parseUser.get("firstName"));
							customer.setLastName((String) parseUser.get("lastName"));
							customer.setAddress((String) parseUser.get("address"));

                            ParseQuery<ParseObject> query = ParseQuery.getQuery("UserServices");
                            query.whereEqualTo("username", customer.getUsername());
                            query.findInBackground(new FindCallback<ParseObject>() {
                                public void done(List<ParseObject> userServiceList, ParseException e) {
                                    if (e == null) {
                                        //Log.d("score", "Retrieved " + scoreList.size() + " scores");
                                        userServiceList.get(0).put("internet", customer.getInternet());
                                        userServiceList.get(0).put("cable", customer.getCable());
                                        userServiceList.get(0).put("phone", customer.getPhone());
                                        userServiceList.get(0).saveInBackground();
                                        Toast.makeText(CustomerServiceAccountActivity.this, "Updated Customer Services",
                                                Toast.LENGTH_LONG).show();
                                    } else {
                                        Toast.makeText(CustomerServiceAccountActivity.this, "Something went wrong",
                                                Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                            /*
							customer.setInternet((String) parseUser.get(("internet")));
							customer.setCable((String) parseUser.get(("cable")));
							customer.setPhone((String) parseUser.get(("phone")));
							*/
							//  account.setPackages((ArrayList<Account.AccountPackage>) parseUser.get("packages"));

							intent.putExtra("currentCustomer", customer);
							startActivity(intent);
						} else
						{
							Toast.makeText(CustomerServiceAccountActivity.this, "User could not be found",
									Toast.LENGTH_LONG).show();
						}
					}
				});
            }
        });

        findViewById(R.id.addPackageButton).setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                username = customerLookup.getText().toString().toLowerCase();
                ParseQuery<ParseUser> query = ParseUser.getQuery();
                query.whereEqualTo("username", username);
                query.findInBackground(new FindCallback<ParseUser>(){
                    @Override
                    public void done(List<ParseUser> parseUsers, ParseException e)
                    {
                        if (e == null)
                        {
                            Intent intent = new Intent(CustomerServiceAccountActivity.this, AddPackageActivity.class);

                            ParseUser parseUser = parseUsers.get(0);

                            // account.setAccountType(Account.AccountType.CUSTOMER_ACCOUNT);
                            customer.setUsername(parseUser.getUsername());
                            customer.setEmail(parseUser.getEmail());
                            customer.setFirstName((String) parseUser.get("firstName"));
                            customer.setLastName((String) parseUser.get("lastName"));
                            customer.setAddress((String) parseUser.get("address"));

                            ParseQuery<ParseObject> query = ParseQuery.getQuery("UserServices");
                            query.whereEqualTo("username", customer.getUsername());
                            query.findInBackground(new FindCallback<ParseObject>() {
                                public void done(List<ParseObject> userServiceList, ParseException e) {
                                    if (e == null) {
                                        //Log.d("score", "Retrieved " + scoreList.size() + " scores");
                                        userServiceList.get(0).put("internet", customer.getInternet());
                                        userServiceList.get(0).put("cable", customer.getCable());
                                        userServiceList.get(0).put("phone", customer.getPhone());
                                        userServiceList.get(0).saveInBackground();
                                    } else {
                                        Toast.makeText(CustomerServiceAccountActivity.this, "Something went wrong",
                                                Toast.LENGTH_LONG).show();
                                    }
                                }
                            });

                            /*
                            customer.setInternet((String) parseUser.get(("internet")));
                            customer.setCable((String) parseUser.get(("cable")));
                            customer.setPhone((String) parseUser.get(("phone")));
                            //  account.setPackages((ArrayList<Account.AccountPackage>) parseUser.get("packages"));
                            */
                            intent.putExtra("currentCustomer", customer);
                            startActivity(intent);
                        } else
                        {
                            Toast.makeText(CustomerServiceAccountActivity.this, "User could not be found",
                                    Toast.LENGTH_LONG).show();
                        }
                    }

                });
            }

        });

	}

	@Override
	protected void onPause()
	{
		super.onPause();
	}

	@Override
	protected void onResume()
	{
		super.onResume();
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_customer_service_account, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings)
		{
			return true;
		}

		return super.onOptionsItemSelected(item);
	}
}
