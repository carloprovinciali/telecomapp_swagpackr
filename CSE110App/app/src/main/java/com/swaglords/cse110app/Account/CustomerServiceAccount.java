package com.swaglords.cse110app.Account;

public class CustomerServiceAccount extends Account
{
	private CustomerAccount customer;

	CustomerServiceAccount ()
	{

	}

	CustomerServiceAccount (CustomerAccount customer)
	{
		this.customer = customer;
	}

    public void addInternet(String internet)
    {
        customer.setInternet(internet);
    }

    public void addCable(String cable)
    {
        customer.setCable(cable);
    }

    public void addPhone(String phone)
    {
        customer.setPhone(phone);
    }

	public void createPackage (AccountPackage newPackage)
	{

	}

	public void createAccount ()
	{

	}
}
