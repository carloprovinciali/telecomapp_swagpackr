package com.swaglords.cse110app.Account;

public class CableService
{
	public static final String NO_CABLE = "No Cable";
	public static final String BASIC_CABLE = "Basic Cable";
	public static final String FOX_NEWS_ONLY = "Fox News Only";
}
