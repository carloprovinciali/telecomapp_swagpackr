package com.swaglords.cse110app.Account;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.List;

public class DatabaseMethods
{
	/*
	public static CustomerAccount getCustomerAccount (String username)
	{
		final BackgroundFlag flag = new BackgroundFlag();
		final CustomerAccount account = new CustomerAccount();

        ParseQuery<ParseUser> query = ParseQuery.getQuery("User");

        query.getInBackground(username.toLowerCase(), new GetCallback<ParseUser>() {
            @Override
            public void done(ParseUser parseUser, ParseException e) {
                if(e == null) {
					account.setAccountType(Account.AccountType.CUSTOMER_ACCOUNT);
					account.setUsername(parseUser.getUsername());
					account.setFirstName((String) parseUser.get("firstName"));
					account.setLastName((String) parseUser.get("lastName"));
					account.setAddress((String) parseUser.get("address"));
					account.setInternet((Account.InternetService) parseUser.get(("internet")));
					account.setCable((Account.CableService) parseUser.get(("cable")));
					account.setPhone((Account.PhoneService) parseUser.get(("phone")));
					account.setPackages((ArrayList<Account.AccountPackage>) parseUser.get("packages"));
					flag.flag = true;
                }
                else {
					flag.flag = false;
                }
            }
        });

		if (flag.flag)
		{
			return account;
		}
		else
		{
			return null;
		}
	}
	*/

    //Signs a user up for an account
    public static void signup(String username, String password, String email, String firstName,
                              String lastName, String address, Context appContext){

        final ProgressDialog dialog = new ProgressDialog(appContext);
        dialog.setTitle("Please wait");
        dialog.setMessage("Please wait while you are signed up");
        dialog.show();

        try {

            final ParseUser user = new ParseUser();
            user.setUsername(username.toLowerCase());
            user.setPassword(password);
            user.setEmail(email);
            user.put("firstName", firstName);
            user.put("lastName", lastName);
            user.put("address", address);

            initializeServices(username);
            user.signUpInBackground();
        }
        catch (Exception e){
            dialog.dismiss();
            Toast.makeText(appContext, "Something went wrong", Toast.LENGTH_LONG).show();
        }

    }

    //Used as a helper method for signing up
    private static void initializeServices(String username){

        try {
            final ParseObject userServices = new ParseObject("UserServices");
            userServices.put("username", username);
            userServices.put("cable", CableService.NO_CABLE);
            userServices.put("internet", InternetService.NO_INTERNET);
            userServices.put("phone", PhoneService.NO_PHONE);
            userServices.put("package", "No Packages");
            userServices.saveInBackground();
        }
        catch (Exception e){
            //Do nothing
        }
    }

    //Used to initialize a CustomerAccount object with values from the database
    public static void getCustomerAccount(CustomerAccount customer, String username){
        ParseQuery<ParseUser>  userQuery = ParseQuery.getQuery("User");
        userQuery.whereEqualTo("username", username);
        List<ParseUser> list = null;
        try{
            list = userQuery.find();
            customer.setCable(list.get(0).getString("cable"));
            customer.setInternet(list.get(0).getString("internet"));
            customer.setPhone(list.get(0).getString("phone"));
        }
        catch(Exception e){
            //Do nothing
        }
    }

	public static void editCustomerAccount (CustomerAccount newAccount)
	{
		ParseUser user = ParseUser.getCurrentUser();
		//user.setUsername(newAccount.getUsername());
		//user.setPassword(newAccount.getPassword());
		//user.setEmail(newAccount.getEmail());
		user.put("firstName", newAccount.getFirstName());
		user.put("lastName", newAccount.getLastName());
		user.put("address", newAccount.getAddress());
		user.put("internet", newAccount.getInternet());
		user.put("cable", newAccount.getCable());
		user.put("phone", newAccount.getPhone());
		//user.put("packages", newAccount.getPackages());

		user.saveInBackground(new SaveCallback()
		{
			@Override
			public void done(ParseException e)
			{
				if (e != null)
				{
					// cri evryteim
                    // cyr fuerver
				}
			}
		});
	}


	/*
	public static String putCustomerAccount (CustomerAccount newAccount)
	{
		final BackgroundFlag flag = new BackgroundFlag();

		ParseUser user = new ParseUser();
		user.setUsername(newAccount.getUsername());
		user.setPassword(newAccount.getPassword());
		user.setEmail(newAccount.getEmail());
		user.put("firstName", newAccount.getFirstName());
		user.put("lastName", newAccount.getLastName());
		user.put("address", newAccount.getAddress());
		//user.put("internet", newAccount.getInternet());
		//user.put("cable", newAccount.getCable());
		//user.put("phone", newAccount.getPhone());
		//user.put("packages", newAccount.getPackages());

		user.signUpInBackground(new SignUpCallback() {
			@Override
			public void done(ParseException e) {
				if(e == null) {
					flag.error = null;
				}
				else {
					flag.error = e.getMessage();
				}
			}
		});

		return flag.error;
	}
	*/
}
