package com.swaglord.cse110telecomrevamp.test;

import com.swaglord.cse110telecomrevamp.Login.MainActivity;
import com.robotium.solo.*;
import android.test.ActivityInstrumentationTestCase2;


public class LoginActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {
    private Solo solo;

    public LoginActivityTest() {
        super(MainActivity.class);
    }

    public void setUp() throws Exception {
        super.setUp();
        solo = new Solo(getInstrumentation());
        getActivity();
    }

    @Override
    public void tearDown() throws Exception {
        solo.finishOpenedActivities();
        super.tearDown();
    }

    public void testRun() {
        //Wait for activity: 'com.swaglord.cse110telecomrevamp.Login.MainActivity'
        solo.waitForActivity(com.swaglord.cse110telecomrevamp.Login.MainActivity.class, 2000);
        //Click on ImageView
        solo.clickOnView(solo.getView(com.swaglord.cse110telecomrevamp.R.id.homeLogin));
        //Wait for activity: 'com.swaglord.cse110telecomrevamp.Login.LoginActivity'
        assertTrue("com.swaglord.cse110telecomrevamp.Login.LoginActivity is not found!", solo.waitForActivity(com.swaglord.cse110telecomrevamp.Login.LoginActivity.class));
        //Click on Empty Text View
        solo.clickOnView(solo.getView(com.swaglord.cse110telecomrevamp.R.id.loginUsername));
        //Enter the text: 'bob'
        solo.clearEditText((android.widget.EditText) solo.getView(com.swaglord.cse110telecomrevamp.R.id.loginUsername));
        solo.enterText((android.widget.EditText) solo.getView(com.swaglord.cse110telecomrevamp.R.id.loginUsername), "bob");
        //Enter the text: 'bob'
        solo.clearEditText((android.widget.EditText) solo.getView(com.swaglord.cse110telecomrevamp.R.id.loginPassword));
        solo.enterText((android.widget.EditText) solo.getView(com.swaglord.cse110telecomrevamp.R.id.loginPassword), "bob");
        //Press next button
        solo.pressSoftKeyboardNextButton();
        //Click on ImageView
        solo.clickOnView(solo.getView(com.swaglord.cse110telecomrevamp.R.id.loginButton));

        //Wait for activity: 'com.swaglord.cse110telecomrevamp.Login.DispatchActivity'
        assertTrue("com.swaglord.cse110telecomrevamp.Login.DispatchActivity is not found!", solo.waitForActivity(com.swaglord.cse110telecomrevamp.Login.DispatchActivity.class));
        solo.sleep(2000);
    }
}
