package com.swaglord.cse110telecomrevamp.AccountActivity;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.swaglord.cse110telecomrevamp.Database.DatabaseMediator;
import com.swaglord.cse110telecomrevamp.R;
import com.swaglord.cse110telecomrevamp.Utilities.Utilities;

public class crepCreateAccountActivity extends ActionBarActivity {
    private EditText username;
    private EditText password;
    private EditText passwordRepeat;
    private EditText email;
    private EditText firstName;
    private EditText lastName;
    private EditText address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crep_create_account);

        //Set up the sign up page
        username = (EditText) findViewById(R.id.signUpUsername);
        password = (EditText) findViewById(R.id.signUpPassword);
        passwordRepeat = (EditText) findViewById(R.id.signUpPasswordRepeat);
        email = (EditText) findViewById(R.id.signUpEmail);
        firstName = (EditText) findViewById(R.id.signUpFirstName);
        lastName = (EditText) findViewById(R.id.signUpLastName);
        address = (EditText) findViewById(R.id.signUpAddress);

        //Set up Register button click handler
        findViewById(R.id.signUpRegisterButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean invalidInformation = false;
                StringBuilder errorMessage =
                        new StringBuilder("Please enter the following fields: ");

                //Check for empty username field
                if (Utilities.isEmpty(username)) {
                    invalidInformation = true;
                    errorMessage.append("username");
                }
                //Check for empty password field
                if (Utilities.isEmpty(password)) {
                    if (invalidInformation) {
                        errorMessage.append(", ");
                    }
                    invalidInformation = true;
                    errorMessage.append("password");
                }
                //Check for matching passwords
                if (!Utilities.isMatching(password, passwordRepeat)) {
                    if (invalidInformation) {
                        errorMessage.append(", ");
                    }
                    invalidInformation = true;
                    errorMessage.append("matching passwords");
                }
                if (Utilities.isEmpty(email)) {
                    if (invalidInformation) {
                        errorMessage.append(", ");
                    }
                    invalidInformation = true;
                    errorMessage.append("email");
                }
                //Check for empty firstName field
                if (Utilities.isEmpty(firstName)) {
                    if (invalidInformation) {
                        errorMessage.append(", ");
                    }
                    invalidInformation = true;
                    errorMessage.append("first name");
                }
                //Check for empty lastName field
                if (Utilities.isEmpty(lastName)) {
                    if (invalidInformation) {
                        errorMessage.append(", ");
                    }
                    invalidInformation = true;
                    errorMessage.append("last name");
                }
                //Check for empty address field
                if (Utilities.isEmpty(address)) {
                    if (invalidInformation) {
                        errorMessage.append(", ");
                    }
                    invalidInformation = true;
                    errorMessage.append("address");
                }
                errorMessage.append(".");

                //If there is an error then display the error
                if (invalidInformation) {
                    Toast.makeText(crepCreateAccountActivity.this, errorMessage.toString()
                            , Toast.LENGTH_LONG).show();
                    return;
                }
                //If username contains a white space then display error
                if (username.getText().toString().contains(" ")) {
                    Toast.makeText(crepCreateAccountActivity.this, "No spaces allowed in username",
                            Toast.LENGTH_LONG).show();
                    return;
                }

                DatabaseMediator.signup(username.getText().toString().toLowerCase().trim(),
                        password.getText().toString(), email.getText().toString().trim(),
                        firstName.getText().toString().trim(), lastName.getText().toString().trim(),
                        address.getText().toString().trim(), false, crepCreateAccountActivity.this);

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_crep_create_account, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
