package com.swaglord.cse110telecomrevamp.Utilities;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parse.ParseFile;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.swaglord.cse110telecomrevamp.R;

import java.text.DecimalFormat;

public class ServiceListAdapter extends ParseQueryAdapter<ParseObject> {

    private static DecimalFormat REAL_FORMATTER = new DecimalFormat("$0.00");

	public ServiceListAdapter(Context context) {
		super(context, new QueryFactory<ParseObject>() {
			public ParseQuery create() {
				ParseQuery query = new ParseQuery("Services");
				return query;
			}
		});
	}

	// Customize the layout by overriding getItemView
	@Override
	public View getItemView(ParseObject object, View v, ViewGroup parent) {
		if (v == null) {
			v = View.inflate(getContext(), R.layout.service_list_view, null);
		}

		super.getItemView(object, v, parent);

        // name
        TextView nameTextView = (TextView) v.findViewById(R.id.name);
        nameTextView.setText(object.getString("name"));

		// type
		TextView typeTextView = (TextView) v.findViewById(R.id.type);
		typeTextView.setText(object.getString("type"));

		// price
		TextView priceTextView = (TextView) v.findViewById(R.id.price);
		priceTextView.setText(REAL_FORMATTER.format(object.getDouble("price")));

		return v;
	}

}
