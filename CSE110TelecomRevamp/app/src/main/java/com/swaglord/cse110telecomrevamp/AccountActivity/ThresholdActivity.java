package com.swaglord.cse110telecomrevamp.AccountActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseException;
import com.swaglord.cse110telecomrevamp.Account.CustomerAccount;
import com.swaglord.cse110telecomrevamp.Database.DatabaseMediator;
import com.swaglord.cse110telecomrevamp.R;

public class ThresholdActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_threshold);

        Context appContext = ThresholdActivity.this;
        Intent intent = getIntent();
        final String username = (String) intent.getSerializableExtra("username");

        double threshVal;
        try {
            threshVal = DatabaseMediator.getThreshold(username);
            TextView displayUsername = (TextView)findViewById(R.id.currentThreshold);
            displayUsername.setText("Current Threshold: " + threshVal);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        final Context finalAppContext = appContext;
        findViewById(R.id.thresholdUpdateButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CustomerAccount account;
                Intent intent = getIntent();
                final String username = (String) intent.getSerializableExtra("username");

                EditText threshold = (EditText) findViewById(R.id.setThreshold);
                String newThreshold = threshold.getText().toString();
                final double thresholdValue = Double.parseDouble(newThreshold);

                try {
                    account = DatabaseMediator.getCustomer(username);

                    if(account.getBill() > thresholdValue) {
                        Toast.makeText(finalAppContext, "Account Balance is above Threshold!",
                                Toast.LENGTH_LONG).show();
                    } else {
                        DatabaseMediator.changeThreshold(username, thresholdValue);
                        Toast.makeText(finalAppContext, "Updated Threshold!",
                                Toast.LENGTH_LONG).show();
                    }



                } catch (ParseException e) {
                    e.printStackTrace();
                }

        }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_threshold, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
