package com.swaglord.cse110telecomrevamp.Utilities;

import android.widget.EditText;

/**
 * Created by Ranger on 3/5/2015.
 */
public class Utilities {
    public static boolean isEmpty(EditText editText){
        return !(editText.getText().toString().trim().length() > 0);
    }

    public static boolean isMatching(EditText editText, EditText editText2){
        return (editText.getText().toString().equals
                (editText2.getText().toString()));
    }
}
