package com.swaglord.cse110telecomrevamp.Account;

import com.swaglord.cse110telecomrevamp.Services.*;
import com.swaglord.cse110telecomrevamp.Services.ServicePackage;

/**
 * Created by Ranger on 3/5/2015.
 */
public class CustomerAccount extends Account {

    public static final double DEFAULT_THRESHOLD = 100;
    private String FirstName;
    private String LastName;
    private double Bill;
    private double BalanceThreshold;
    private ServicePackage ServicePackage;
    private CableService CableService;
    private InternetService InternetService;
    private PhoneService PhoneService;
    private boolean IsRetail;

    public CustomerAccount(String username, String firstName, String lastName, boolean isRetail) {
        this.Username = username;
        this.Type = CUSTOMER;
        this.FirstName = firstName;
        this.LastName = lastName;
        this.BalanceThreshold = DEFAULT_THRESHOLD;
        this.ServicePackage = new ServicePackage();
        this.CableService = new CableService();
        this.InternetService = new InternetService();
        this.PhoneService = new PhoneService();
        this.IsRetail = isRetail;
    }

    //Getters and setters

    public double getBill() {
        return Bill;
    }

    public void setBill(double bill) {
        Bill = bill;
    }

    public double getBalanceThreshold() {
        return BalanceThreshold;
    }

    public void setBalanceThreshold(double balanceThreshold) {
        BalanceThreshold = balanceThreshold;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public CableService getCableService() {
        return CableService;
    }

    public void setCableService(CableService cableService) {
        CableService = cableService;
    }

    public InternetService getInternetService() {
        return InternetService;
    }

    public void setInternetService(InternetService internetService) {
        InternetService = internetService;
    }

    public PhoneService getPhoneService() {
        return PhoneService;
    }

    public void setPhoneService(PhoneService phoneService) {
        PhoneService = phoneService;
    }

    public ServicePackage getServicePackage() {
        return ServicePackage;
    }

    public void setServicePackage(ServicePackage servicePackage) {
        ServicePackage = servicePackage;
    }

    public boolean isRetail() {
        return IsRetail;
    }

    public void setRetail(boolean isRetail) {
        IsRetail = isRetail;
    }
}
