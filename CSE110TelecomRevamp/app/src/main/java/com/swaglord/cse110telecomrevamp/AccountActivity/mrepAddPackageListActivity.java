package com.swaglord.cse110telecomrevamp.AccountActivity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.swaglord.cse110telecomrevamp.Account.MarketingRepresentativeAccount;
import com.swaglord.cse110telecomrevamp.R;

public class mrepAddPackageListActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mrep_add_package_list);

        findViewById(R.id.mrepAddPackageListUpdateButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText packageName = (EditText) findViewById(R.id.mrepAddPackageListName);
                EditText cableName = (EditText) findViewById(R.id.mrepAddPackageListCable);
                EditText internetName = (EditText) findViewById(R.id.mrepAddPackageListInternet);
                EditText phoneName = (EditText) findViewById(R.id.mrepAddPackageListPhone);
                EditText packagePrice = (EditText) findViewById(R.id.mrepAddPackageListPrice);

                String package_name = packageName.getText().toString();
                String cable_name = cableName.getText().toString();
                String internet_name = internetName.getText().toString();
                String phone_name = phoneName.getText().toString();
                double price_amount = Double.parseDouble(packagePrice.getText().toString());

                try {
                    boolean success = MarketingRepresentativeAccount.addPackage(package_name, cable_name, internet_name, phone_name, price_amount);
                    if(success == false) {
                        Context context = getApplicationContext();
                        CharSequence text = "Please enter valid information";
                        int duration = Toast.LENGTH_LONG;
                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                    }
                } catch (ParseException e) {
                    Intent intent =
                            new Intent(mrepAddPackageListActivity.this, mrepEditPackageListActivity.class);
                    startActivity(intent);
                }

                Intent intent =
                        new Intent(mrepAddPackageListActivity.this, mrepEditPackageListActivity.class);
                startActivity(intent);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mrep_add_package_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
