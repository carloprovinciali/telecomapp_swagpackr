package com.swaglord.cse110telecomrevamp.Account;

import android.content.Context;
import android.widget.Toast;

import com.parse.ParseException;
import com.swaglord.cse110telecomrevamp.AccountActivity.mrepEditServiceListActivity;
import com.swaglord.cse110telecomrevamp.Database.DatabaseMediator;
import com.swaglord.cse110telecomrevamp.Services.*;
import com.swaglord.cse110telecomrevamp.Services.ServicePackage;

/**
 * Created by Tyler on 3/8/2015.
 */
public class MarketingRepresentativeAccount extends Account {

    /* constructor */
    public MarketingRepresentativeAccount (String username) {
        this.Username = username;
        this.Type = Account.MARKETING_REP;
    }

    public static boolean addService(String type, String name, double price) {
        boolean method_success = false;

        if(type.equals(Service.CABLE)) {
            CableService cable = new CableService(name, price);
            return DatabaseMediator.createService(cable);
        }
        if(type.equals(Service.INTERNET)) {
            InternetService internet = new InternetService(name, price);
            return DatabaseMediator.createService(internet);
        }
        if(type.equals(Service.PHONE)) {
            PhoneService phone = new PhoneService(name, price);
            return DatabaseMediator.createService(phone);
        }

        return method_success;
    }

    public static boolean removeService(String type, String name) throws ParseException {
        boolean method_success = false;

        if(type.equals(Service.CABLE)) {
            CableService cable = new CableService(name, 0);
            return DatabaseMediator.removeService(cable);
        }
        if(type.equals(Service.INTERNET)) {
            InternetService internet = new InternetService(name, 0);
            return DatabaseMediator.removeService(internet);
        }
        if(type.equals(Service.PHONE)) {
            PhoneService phone = new PhoneService(name, 0);
            return DatabaseMediator.removeService(phone);
        }

        return method_success;
    }

    public static boolean addPackage(String name, String cable, String internet, String phone, double price) throws ParseException {
        CableService cableService = (CableService) DatabaseMediator.getService(cable, Service.CABLE);
        InternetService internetService = (InternetService) DatabaseMediator.getService(internet, Service.INTERNET);
        PhoneService phoneService = (PhoneService) DatabaseMediator.getService(phone, Service.PHONE);

        if(cableService == null || internetService == null || phoneService == null) {
            return false;
        }
        else {
            ServicePackage servicePackage = new ServicePackage(name, price, cableService, internetService, phoneService);
            return DatabaseMediator.addPackage(servicePackage);
        }
    }

    public static boolean removePackage(String name) throws ParseException {
        return DatabaseMediator.removePackage(name);
    }

    public static boolean addToPackage(String package_name, String service_type, String service_name) throws ParseException {

        if(service_type.equals(Service.CABLE)) {
            CableService cable = new CableService(service_name, 0);
            return DatabaseMediator.addToPackage(cable, package_name);
        }
        if(service_type.equals(Service.INTERNET)) {
            InternetService internet = new InternetService(service_name, 0);
            return DatabaseMediator.addToPackage(internet, package_name);
        }
        if(service_type.equals(Service.PHONE)) {
            PhoneService phone = new PhoneService(service_name, 0);
            return DatabaseMediator.addToPackage(phone, package_name);
        }

        return false;
    }

    public static boolean removeFromPackage(String package_name, String service_type, String service_name) throws ParseException {

        if(service_type.equals(Service.CABLE)) {
            CableService cable = new CableService(service_name, 0);
            return DatabaseMediator.removeFromPackage(cable, package_name);
        }
        if(service_type.equals(Service.INTERNET)) {
            InternetService internet = new InternetService(service_name, 0);
            return DatabaseMediator.removeFromPackage(internet, package_name);
        }
        if(service_type.equals(Service.PHONE)) {
            PhoneService phone = new PhoneService(service_name, 0);
            return DatabaseMediator.removeFromPackage(phone, package_name);
        }

        return false;
    }

    public static boolean changePrice(String package_name, double price) throws ParseException {

        return DatabaseMediator.changePrice(package_name, price);
    }
}
