package com.swaglord.cse110telecomrevamp.Services;

/**
 * Created by Ranger on 3/5/2015.
 */
public class PhoneService extends Service {
    public static final String NO_PHONE = "No Phone Service";
    public PhoneService(){
        this.Name = NO_PHONE;
        this.Price = null;
        this.Type = "phone";
    }
    public PhoneService(String name, double price){
        this.Name = name;
        this.Price = price;
        this.Type = "phone";
    }
}
