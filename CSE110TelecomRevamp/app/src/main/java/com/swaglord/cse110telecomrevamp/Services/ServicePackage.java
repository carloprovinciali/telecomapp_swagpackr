package com.swaglord.cse110telecomrevamp.Services;

/**
 * Created by Ranger on 3/5/2015.
 */
public class ServicePackage extends Service{
    private Service CableService;
    private Service InternetService;
    private Service PhoneService;
    public static final String NO_PACKAGE = "No Package";

    public ServicePackage(){
        this.Name = NO_PACKAGE;
        this.Price = null;
        this.Type = PACKAGE;
        this.CableService = new CableService();
        this.InternetService = new InternetService();
        this.PhoneService = new PhoneService();
    }

    public ServicePackage(String name, Double price, CableService cableService,
                          InternetService internetService, PhoneService phoneService){
        this.Name = name;
        this.Price = price;
        this.Type = PACKAGE;
        this.CableService = cableService;
        this.InternetService = internetService;
        this.PhoneService = phoneService;
    }

    public Service getCableService() {
        return CableService;
    }

    public void setCableService(Service cableService) {
        CableService = cableService;
    }

    public Service getInternetService() {
        return InternetService;
    }

    public void setInternetService(Service internetService) {
        InternetService = internetService;
    }

    public Service getPhoneService() {
        return PhoneService;
    }

    public void setPhoneService(Service phoneService) {
        PhoneService = phoneService;
    }

}
