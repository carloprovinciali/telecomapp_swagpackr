package com.swaglord.cse110telecomrevamp.AccountActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.parse.ParseException;
import com.swaglord.cse110telecomrevamp.Account.MarketingRepresentativeAccount;
import com.swaglord.cse110telecomrevamp.R;

public class mrepChangePriceActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_price);

        Bundle extras = getIntent().getExtras();
        //if (extras != null) {
        final String package_name = extras.getString("package_name");
        //}

        findViewById(R.id.priceButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            EditText price_amount = (EditText) findViewById(R.id.priceAmount);
            double price = Double.parseDouble(price_amount.getText().toString());

            try {
                MarketingRepresentativeAccount.changePrice(package_name, price);
            } catch (ParseException e) {

            }

            Intent i =
            new Intent(mrepChangePriceActivity.this, mrepEditPackageActivity.class);
            i.putExtra("package_name", package_name);
            startActivity(i);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mrep_select_edit_package, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
