package com.swaglord.cse110telecomrevamp.AccountActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.parse.Parse;
import com.parse.ParseException;
import com.swaglord.cse110telecomrevamp.Account.CustomerAccount;
import com.swaglord.cse110telecomrevamp.Database.DatabaseMediator;
import com.swaglord.cse110telecomrevamp.R;

public class CustomerActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer);
        Parse.initialize(this, "KUM3rYteX0e8MyGbaOYeTusGixjP8DZ3aFDnLVTR", "Rd6buwRBPPQHMQLmvqfLFgq2TtZiKjKoh0LS3or2");
        
        CustomerAccount account;
        Intent intent = getIntent();
        final String username = (String) intent.getSerializableExtra("username");

        try {
            account = DatabaseMediator.getCustomer(username);
            TextView displayUsername = (TextView)findViewById(R.id.cusActivityBottomBar);
            displayUsername.setText("Username: " + account.getUsername());
        } catch (ParseException e) {
            e.printStackTrace();
        }


        // my services button
        findViewById(R.id.myServicesButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =
                        new Intent(CustomerActivity.this, MyServicesActivity.class);
                intent.putExtra("username", username);
                startActivity(intent);
            }
        });

        // shop all button
        findViewById(R.id.shopAllButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =
                        new Intent(CustomerActivity.this, ViewServicePackageActivity.class);
                intent.putExtra("username", username);
                startActivity(intent);
            }
        });

        // view bill button link
        findViewById(R.id.viewBillButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =
                        new Intent(CustomerActivity.this, BillingActivity.class);
                intent.putExtra("username", username);
                startActivity(intent);
            }
        });

        // settings button (threshold)
        findViewById(R.id.settingsButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =
                        new Intent(CustomerActivity.this, ThresholdActivity.class);
                intent.putExtra("username", username);
                startActivity(intent);
            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_customer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
