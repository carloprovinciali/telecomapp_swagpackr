package com.swaglord.cse110telecomrevamp.AccountActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseException;
import com.swaglord.cse110telecomrevamp.Account.CustomerAccount;
import com.swaglord.cse110telecomrevamp.Database.DatabaseMediator;
import com.swaglord.cse110telecomrevamp.R;

public class BillingActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_billing);

        Context appContext = BillingActivity.this;
        CustomerAccount account;
        Intent intent = getIntent();
        final String username = (String) intent.getSerializableExtra("username");
        double thresholdValue;
        try {
            // gets customer information
            account = DatabaseMediator.getCustomer(username);

            double internetPrice = account.getInternetService().getPrice();
            double phonePrice = account.getPhoneService().getPrice();
            double cablePrice = account.getCableService().getPrice();
            double packagePrice = account.getServicePackage().getPrice();
            double totalPrice = 0;

            if(packagePrice == 0) {
                totalPrice = internetPrice + phonePrice + cablePrice;
                account.setBill(totalPrice);
            } else {
                totalPrice = packagePrice;
                account.setBill(totalPrice);
                phonePrice = 0;
                cablePrice = 0;
                internetPrice = 0;
            }

            thresholdValue = DatabaseMediator.getThreshold(username);
            if(totalPrice > thresholdValue) {
                final Context finalAppContext = appContext;
                Toast.makeText(finalAppContext, "Account Balance is above Threshold!",
                        Toast.LENGTH_LONG).show();
            }

            // display info
            TextView displayPackage = (TextView)findViewById(R.id.displayPackage);
            displayPackage.setText(account.getServicePackage().getName());

            TextView displayInternet = (TextView)findViewById(R.id.displayInternet);
            displayInternet.setText(account.getInternetService().getName());

            TextView displayPhone = (TextView)findViewById(R.id.displayPhone);
            displayPhone.setText(account.getPhoneService().getName());

            TextView displayCable = (TextView)findViewById(R.id.displayCable);
            displayCable.setText(account.getCableService().getName());

            TextView displayPackagePrice = (TextView)findViewById(R.id.displayPackagePrice);
            displayPackagePrice.setText("$" + packagePrice);

            TextView displayInternetPrice = (TextView)findViewById(R.id.displayInternetPrice);
            displayInternetPrice.setText("$" + internetPrice);

            TextView displayPhonePrice = (TextView)findViewById(R.id.displayPhonePrice);
            displayPhonePrice.setText("$" + phonePrice);

            TextView displayCablePrice = (TextView)findViewById(R.id.displayCablePrice);
            displayCablePrice.setText("$" + cablePrice);

            TextView displayTotal = (TextView)findViewById(R.id.theTotalPrice);
            displayTotal.setText("$" + totalPrice);


        } catch (ParseException e) {
            e.printStackTrace();
        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_billing, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
