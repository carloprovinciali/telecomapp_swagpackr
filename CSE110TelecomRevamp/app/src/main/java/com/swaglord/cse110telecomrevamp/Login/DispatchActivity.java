package com.swaglord.cse110telecomrevamp.Login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.parse.ParseUser;
import com.swaglord.cse110telecomrevamp.Account.Account;
import com.swaglord.cse110telecomrevamp.AccountActivity.CustomerActivity;
import com.swaglord.cse110telecomrevamp.AccountActivity.CustomerRepresentativeActivity;
import com.swaglord.cse110telecomrevamp.AccountActivity.MarketingRepresentativeActivity;
import com.swaglord.cse110telecomrevamp.R;

public class DispatchActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(ParseUser.getCurrentUser() != null && getIntent() != null){
            Intent intent = getIntent();
            final String userType = (String) intent.getSerializableExtra("type");
            final String username = (String) intent.getSerializableExtra("username");
            if(userType.equals(Account.CUSTOMER)){
                Intent customerIntent = new Intent(this, CustomerActivity.class);
                customerIntent.putExtra("username", username);
                customerIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(customerIntent);
            }
            else if(userType.equals(Account.CUSTOMER_REP)){
                Intent customerRepIntent = new Intent(this, CustomerRepresentativeActivity.class);
                customerRepIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(customerRepIntent);
            }
            else if(userType.equals(Account.MARKETING_REP)){
                Intent marketingRepIntent = new Intent(this, MarketingRepresentativeActivity.class);
                marketingRepIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(marketingRepIntent);
            }
            else{
                startActivity(new Intent(this, MainActivity.class));
            }
        }
        else{
            startActivity(new Intent(this, MainActivity.class));
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_dispatch, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
