package com.swaglord.cse110telecomrevamp.AccountActivity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseException;
import com.swaglord.cse110telecomrevamp.Account.MarketingRepresentativeAccount;
import com.swaglord.cse110telecomrevamp.Database.DatabaseMediator;
import com.swaglord.cse110telecomrevamp.R;
import com.swaglord.cse110telecomrevamp.Services.CableService;
import com.swaglord.cse110telecomrevamp.Services.InternetService;
import com.swaglord.cse110telecomrevamp.Services.PhoneService;
import com.swaglord.cse110telecomrevamp.Services.Service;

public class mrepAddPackageActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mrep_add_package);

        Bundle extras = getIntent().getExtras();
        //if (extras != null) {
        final String package_name = extras.getString("package_name");
        //}

        findViewById(R.id.AddToPackageButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Service existCheck = null;

                EditText packageName = (EditText) findViewById(R.id.AddPackageName);
                EditText serviceType = (EditText) findViewById(R.id.AddServiceType);
                EditText serviceName = (EditText) findViewById(R.id.AddServiceName);

                String package_name = packageName.getText().toString();
                String service_type = serviceType.getText().toString();
                String service_name = serviceName.getText().toString();

                try {
                    existCheck = DatabaseMediator.getService(service_name, service_type);
                } catch (ParseException e) {

                }
                // add service
                if(existCheck != null) {
                    try {
                        boolean success = MarketingRepresentativeAccount.addToPackage(package_name, service_type, service_name);
                        if(success == false) {
                            Context context = getApplicationContext();
                            CharSequence text = "Please enter valid information";
                            int duration = Toast.LENGTH_LONG;
                            Toast toast = Toast.makeText(context, text, duration);
                            toast.show();
                        }
                    } catch (ParseException e) {

                    }
                } else {
                    Context context = getApplicationContext();
                    CharSequence text = "Please enter valid information";
                    int duration = Toast.LENGTH_LONG;
                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                }

                Intent i =
                        new Intent(mrepAddPackageActivity.this, mrepEditPackageActivity.class);
                i.putExtra("package_name", package_name);
                startActivity(i);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mrep_add_package, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
