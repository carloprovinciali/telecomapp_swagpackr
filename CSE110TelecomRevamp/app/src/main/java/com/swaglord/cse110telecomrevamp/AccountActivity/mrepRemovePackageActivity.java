package com.swaglord.cse110telecomrevamp.AccountActivity;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.parse.ParseException;
import com.swaglord.cse110telecomrevamp.Account.MarketingRepresentativeAccount;
import com.swaglord.cse110telecomrevamp.Database.DatabaseMediator;
import com.swaglord.cse110telecomrevamp.R;
import com.swaglord.cse110telecomrevamp.Services.Service;

public class mrepRemovePackageActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mrep_remove_package);

        Bundle extras = getIntent().getExtras();
        //if (extras != null) {
        final String package_name = extras.getString("package_name");
        //}

        findViewById(R.id.RemovePackageButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText packageName = (EditText) findViewById(R.id.RemovePackageName);
                EditText serviceType = (EditText) findViewById(R.id.RemoveServiceType);
                EditText serviceName = (EditText) findViewById(R.id.RemoveServiceName);

                String package_name = packageName.getText().toString();
                String service_type = serviceType.getText().toString();
                String service_name = serviceName.getText().toString();

                // add service
                try {
                    MarketingRepresentativeAccount.removeFromPackage(package_name, service_type, service_name);
                } catch (ParseException e) {

                }

                Intent i =
                        new Intent(mrepRemovePackageActivity.this, mrepEditPackageActivity.class);
                i.putExtra("package_name", package_name);
                startActivity(i);
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mrep_remove_package, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
