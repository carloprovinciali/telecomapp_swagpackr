package com.swaglord.cse110telecomrevamp.AccountActivity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.swaglord.cse110telecomrevamp.Account.MarketingRepresentativeAccount;
import com.swaglord.cse110telecomrevamp.R;

public class mrepAddServiceActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mrep_add_service);

        findViewById(R.id.mrepAddServiceUpdateButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText serviceType = (EditText) findViewById(R.id.mrepAddServiceType);
                EditText serviceName = (EditText) findViewById(R.id.mrepAddServiceName);
                EditText servicePrice = (EditText) findViewById(R.id.mrepAddServicePrice);

                String type = serviceType.getText().toString();
                String name = serviceName.getText().toString();
                double price = Double.parseDouble(servicePrice.getText().toString());

                boolean success = MarketingRepresentativeAccount.addService(type, name, price);

                if(success == false) {
                    Context context = getApplicationContext();
                    CharSequence text = "Please enter valid information";
                    int duration = Toast.LENGTH_LONG;
                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                }

                Intent intent =
                        new Intent(mrepAddServiceActivity.this, mrepEditServiceListActivity.class);
                startActivity(intent);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mrep_add, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
