package com.swaglord.cse110telecomrevamp.AccountActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.parse.ParseException;
import com.swaglord.cse110telecomrevamp.Account.CustomerAccount;
import com.swaglord.cse110telecomrevamp.Database.DatabaseMediator;
import com.swaglord.cse110telecomrevamp.R;

public class MyServicesActivity extends ActionBarActivity {

    private String cable;
    private double cablePrice;
    private String internet;
    private double internetPrice;
    private String phone;
    private double phonePrice;
    private String packageName;
    private double packagePrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_services);

        CustomerAccount account;
        Intent intent = getIntent();
        final String username = (String) intent.getSerializableExtra("username");
        try {
            // gets customer information
            account = DatabaseMediator.getCustomer(username);
            TextView acctType = (TextView) findViewById(R.id.textView30);
            if(account.isRetail()) {
                acctType.setText("Retail Customer");
            } else {
                acctType.setText("Commercial Customer");
            }

            // display info
            TextView displayPackage = (TextView)findViewById(R.id.myServicesPackageName);
            displayPackage.setText(account.getServicePackage().getName() + "     $" +
                                    account.getServicePackage().getPrice());

            TextView displayCable = (TextView)findViewById(R.id.myServicesCableName);
            displayCable.setText(account.getCableService().getName() + "     $" +
                                    account.getCableService().getPrice());

            TextView displayPhone = (TextView)findViewById(R.id.myServicesPhoneName);
            displayPhone.setText(account.getPhoneService().getName() + "     $" +
                                    account.getPhoneService().getPrice());

            TextView displayInternet = (TextView)findViewById(R.id.myServicesInternetName);
            displayInternet.setText(account.getInternetService().getName() + "     $" +
                                        account.getInternetService().getPrice());


        } catch (ParseException e) {
            e.printStackTrace();
        }

        findViewById(R.id.deleteButton2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyServicesActivity.this, CustomerDeleteActivity.class);
                intent.putExtra("username", username);
                startActivity(intent);
            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my_services, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
