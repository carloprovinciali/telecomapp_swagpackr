package com.swaglord.cse110telecomrevamp.AccountActivity;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import com.parse.ParseException;
import com.swaglord.cse110telecomrevamp.Database.DatabaseMediator;
import com.swaglord.cse110telecomrevamp.R;
import com.swaglord.cse110telecomrevamp.Utilities.ServiceListAdapter;

public class crepServiceActivity extends ActionBarActivity {
    private EditText serviceToAdd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crep_service);

        Intent intent = getIntent();
        final String username = (String) intent.getSerializableExtra("username");

        // Initialize the subclass of ParseQueryAdapter
        ServiceListAdapter serviceListAdapter = new ServiceListAdapter(this);

        // Initialize ListView
        ListView serviceList = (ListView) findViewById(R.id.addServiceListView);
        serviceList.setAdapter(serviceListAdapter);

        serviceToAdd = (EditText) findViewById(R.id.addServiceServiceName);

        findViewById(R.id.addServiceButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //user.set
                //String service = serviceToAdd.getText().toString();
                try {
                    // add service using DB mediator
                    DatabaseMediator.setNewService(serviceToAdd.getText().toString(), username,
                            crepServiceActivity.this);

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_crep_service, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
