package com.swaglord.cse110telecomrevamp.Utilities;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.swaglord.cse110telecomrevamp.R;

import java.text.DecimalFormat;

public class PackageListAdapter extends ParseQueryAdapter<ParseObject> {

    private static DecimalFormat REAL_FORMATTER = new DecimalFormat("$0.00");

	public PackageListAdapter(Context context) {
		super(context, new QueryFactory<ParseObject>() {
			public ParseQuery create() {
				ParseQuery query = new ParseQuery("Packages");
				return query;
			}
		});
	}

	// Customize the layout by overriding getItemView
	@Override
	public View getItemView(ParseObject object, View v, ViewGroup parent) {
		if (v == null) {
			v = View.inflate(getContext(), R.layout.package_list_view, null);
		}

		super.getItemView(object, v, parent);

        // name
        TextView nameTextView = (TextView) v.findViewById(R.id.name);
        nameTextView.setText(object.getString("name"));

		// cable
		TextView cableTextView = (TextView) v.findViewById(R.id.cable);
		cableTextView.setText(object.getString("cable"));

        // internet
        TextView internetTextView = (TextView) v.findViewById(R.id.internet);
        internetTextView.setText(object.getString("internet"));

        // phone
        TextView phoneTextView = (TextView) v.findViewById(R.id.phone);
        phoneTextView.setText(object.getString("phone"));

		// price
		TextView priceTextView = (TextView) v.findViewById(R.id.price);
		priceTextView.setText(REAL_FORMATTER.format(object.getDouble("price")));

		return v;
	}

}
