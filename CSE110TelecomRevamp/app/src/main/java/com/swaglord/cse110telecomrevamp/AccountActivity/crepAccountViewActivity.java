package com.swaglord.cse110telecomrevamp.AccountActivity;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


import com.parse.ParseException;
import com.swaglord.cse110telecomrevamp.Account.CustomerRepresentativeAccount;
import com.swaglord.cse110telecomrevamp.R;

public class crepAccountViewActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crep_account_view);

        Intent intent = getIntent();
        final String username = (String) intent.getSerializableExtra("username");
        CustomerRepresentativeAccount crep = new CustomerRepresentativeAccount("customer representative");
        try {
            crep.lookUp(username);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        TextView acctType = (TextView) findViewById(R.id.textViewBottom);
        if(crep.account.isRetail()){
            acctType.setText("Retail Customer");
        } else {
            acctType.setText("Commercial Customer");
        }

        // display info
        TextView displayPackage = (TextView)findViewById(R.id.servicesPackageName);
        displayPackage.setText(crep.account.getServicePackage().getName());

        TextView displayCable = (TextView)findViewById(R.id.servicesCableName);
        displayCable.setText(crep.account.getCableService().getName() );

        TextView displayPhone = (TextView)findViewById(R.id.servicesPhoneName);
        displayPhone.setText(crep.account.getPhoneService().getName() );

        TextView displayInternet = (TextView)findViewById(R.id.servicesInternetName);
        displayInternet.setText(crep.account.getInternetService().getName());

        TextView displayUsername = (TextView)findViewById(R.id.textViewTop);
        displayUsername.setText("Username :" + crep.account.getUsername());

        // buttons do the stuff
        findViewById(R.id.addServiceButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(crepAccountViewActivity.this, crepServiceActivity.class);
                intent.putExtra("username", username);
                startActivity(intent);
            }
        });

        findViewById(R.id.addPackageButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Intent intent = new Intent(crepAccountViewActivity.this, crepPackageActivity.class);
                intent.putExtra("username", username);
                startActivity(intent);
            }
        });

        findViewById(R.id.deleteStuff).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(crepAccountViewActivity.this, crepDeleteActivity.class);
                intent.putExtra("username", username);
                startActivity(intent);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_crep_account_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
