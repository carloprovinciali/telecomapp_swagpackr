package com.swaglord.cse110telecomrevamp.AccountActivity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseException;
import com.swaglord.cse110telecomrevamp.Database.DatabaseMediator;
import com.swaglord.cse110telecomrevamp.R;
import com.swaglord.cse110telecomrevamp.Services.ServicePackage;
import com.swaglord.cse110telecomrevamp.Utilities.ServiceListAdapter;

import java.text.DecimalFormat;

public class mrepEditPackageActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mrep_edit_package);

       // String packageName = null;

        Bundle extras = getIntent().getExtras();
        //if (extras != null) {
            final String packageName = extras.getString("package_name");
        //}
        ServicePackage servicePackage = null;
        DecimalFormat REAL_FORMATTER = new DecimalFormat("$0.00");

        try {
            servicePackage = DatabaseMediator.getPackage(packageName);
            if (servicePackage == null) {
                Context context = getApplicationContext();
                CharSequence text = "Please enter valid information";
                int duration = Toast.LENGTH_LONG;
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
                Intent intent =
                        new Intent(mrepEditPackageActivity.this, mrepSelectEditPackageActivity.class);
                startActivity(intent);
            }
        } catch (ParseException e) {
            Intent intent =
                    new Intent(mrepEditPackageActivity.this, mrepSelectEditPackageActivity.class);
            startActivity(intent);
        }

        if (servicePackage != null) {
            //package name
            TextView displayPackageName = (TextView) findViewById(R.id.packageName);
            displayPackageName.setText(servicePackage.getName());

            //cable
            TextView displayCableName = (TextView) findViewById(R.id.cableName);
            displayCableName.setText(servicePackage.getCableService().getName());

            TextView displayCableType = (TextView) findViewById(R.id.cableType);
            displayCableType.setText(servicePackage.getCableService().getType());

            //internet
            TextView displayInternetName = (TextView) findViewById(R.id.internetName);
            displayInternetName.setText(servicePackage.getInternetService().getName());

            TextView displayInternetType = (TextView) findViewById(R.id.internetType);
            displayInternetType.setText(servicePackage.getInternetService().getType());

            //phone
            TextView displayPhoneName = (TextView) findViewById(R.id.phoneName);
            displayPhoneName.setText(servicePackage.getPhoneService().getName());

            TextView displayPhoneType = (TextView) findViewById(R.id.phoneType);
            displayPhoneType.setText(servicePackage.getPhoneService().getType());

            //package price
            TextView displayPackagePrice = (TextView) findViewById(R.id.packagePrice);
            displayPackagePrice.setText(REAL_FORMATTER.format(servicePackage.getPrice()));
        }

        findViewById(R.id.mrepAddPackageButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =
                        new Intent(mrepEditPackageActivity.this, mrepAddPackageActivity.class);
                i.putExtra("package_name", packageName);
                startActivity(i);
            }
        });

        findViewById(R.id.mrepRemovePackageButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =
                        new Intent(mrepEditPackageActivity.this, mrepRemovePackageActivity.class);
                i.putExtra("package_name", packageName);
                startActivity(i);
            }
        });

        findViewById(R.id.mrepChangePriceButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =
                        new Intent(mrepEditPackageActivity.this, mrepChangePriceActivity.class);
                i.putExtra("package_name", packageName);
                startActivity(i);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mrep_edit_package, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent selectIntent = new Intent(this, mrepSelectEditPackageActivity.class);
        selectIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(selectIntent);
    }
}
