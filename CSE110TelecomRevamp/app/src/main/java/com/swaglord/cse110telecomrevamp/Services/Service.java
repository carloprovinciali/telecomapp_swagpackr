package com.swaglord.cse110telecomrevamp.Services;

import com.swaglord.cse110telecomrevamp.R;

import java.io.Serializable;

/**
 * Created by Ranger on 3/5/2015.
 */
public abstract class Service implements Serializable {
    protected String Name;
    protected Double Price;
    protected String Type;
    public static final String CABLE = "cable";
    public static final String INTERNET = "internet";
    public static final String PHONE = "phone";
    public static final String PACKAGE = "package";

    public static Service createService(String name, String serviceType, Double price) throws Exception{
        if(CABLE.equals(serviceType)){
            return new CableService(name, price);
        }
        else if(INTERNET.equals(serviceType)){
            return new InternetService(name, price);
        }
        else if(PHONE.equals(serviceType)){
            return new PhoneService(name, price);
        }
        else{
            throw new Exception(R.string.unknown_service + serviceType);
        }
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Double getPrice() {
        return Price;
    }

    public void setPrice(Double price) {
        Price = price;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }
}
