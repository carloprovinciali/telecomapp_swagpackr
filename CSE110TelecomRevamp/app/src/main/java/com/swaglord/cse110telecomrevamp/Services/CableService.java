package com.swaglord.cse110telecomrevamp.Services;

/**
 * Created by Ranger on 3/5/2015.
 */
public class CableService extends Service {
    public static final String NO_CABLE = "No Cable Service";

    public CableService(){
        this.Name = NO_CABLE;
        this.Price = null;
        this.Type = "cable";
    }
    public CableService(String name, double price){
        this.Name = name;
        this.Price = price;
        this.Type = "cable";
    }
}
