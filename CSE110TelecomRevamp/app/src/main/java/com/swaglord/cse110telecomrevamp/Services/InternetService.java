package com.swaglord.cse110telecomrevamp.Services;

/**
 * Created by Ranger on 3/5/2015.
 */
public class InternetService extends Service {
    public static final String NO_INTERNET = "No Internet Service";

    public InternetService(){
        this.Name = NO_INTERNET;
        this.Price = null;
        this.Type = "cable";
    }
    public InternetService(String name, double price){
        this.Name = name;
        this.Price = price;
        this.Type = "internet";
    }
}
