package com.swaglord.cse110telecomrevamp.Account;

import java.io.Serializable;

/**
 * Created by Ranger on 3/5/2015.
 */
public class Account implements Serializable{
    public static final String CUSTOMER = "customer";
    public static final String CUSTOMER_REP = "customer representative";
    public static final String MARKETING_REP = "marketing representative";
    protected String Username;
    protected String Type;

    public String getUsername(){
        return Username;
    }

    public String getType(){
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }
}
