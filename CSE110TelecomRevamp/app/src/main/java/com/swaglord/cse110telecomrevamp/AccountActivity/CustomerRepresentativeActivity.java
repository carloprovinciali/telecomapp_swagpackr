package com.swaglord.cse110telecomrevamp.AccountActivity;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.swaglord.cse110telecomrevamp.Account.CustomerRepresentativeAccount;
import com.swaglord.cse110telecomrevamp.Account.CustomerAccount;
import com.swaglord.cse110telecomrevamp.Database.DatabaseMediator;
import com.swaglord.cse110telecomrevamp.R;
import com.swaglord.cse110telecomrevamp.Services.CableService;
import com.swaglord.cse110telecomrevamp.Services.InternetService;
import com.swaglord.cse110telecomrevamp.Services.PhoneService;
import com.swaglord.cse110telecomrevamp.Services.ServicePackage;
import com.swaglord.cse110telecomrevamp.Utilities.Utilities;

import java.util.List;

public class CustomerRepresentativeActivity extends ActionBarActivity {


    /*TODO
    lookup existing account -- search bar
        view customer account information
            add services activity
            delete services
    create account -- button
        customer rep create account activity
        call dbmediator.signup in new account create activity or reuse signup activity
    */

    private EditText customerLookup;
    private CustomerAccount account;
    private String username;
    //private ParseUser parseUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Parse.initialize(this, "KUM3rYteX0e8MyGbaOYeTusGixjP8DZ3aFDnLVTR", "Rd6buwRBPPQHMQLmvqfLFgq2TtZiKjKoh0LS3or2");
        setContentView(R.layout.activity_customer_representative);
        customerLookup = (EditText) findViewById(R.id.lookupText);

        //Intent intent = getIntent();

        findViewById(R.id.CreateAccount).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Intent intent = new Intent(CustomerRepresentativeActivity.this, crepCreateAccountActivity.class);
                startActivity(intent);
            }

        });

        findViewById(R.id.lookupButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean invalidInformation = false;
                StringBuilder errorMessage =
                        new StringBuilder("Please enter the following fields: ");

                if (Utilities.isEmpty(customerLookup)) {
                    invalidInformation = true;
                    errorMessage.append("lookup");
                }
                errorMessage.append(".");

                if(invalidInformation){
                    Toast.makeText(CustomerRepresentativeActivity.this, errorMessage.toString(), Toast.LENGTH_LONG).show();
                    return;
                } else {

                }

                username = customerLookup.getText().toString().toLowerCase();
                CustomerRepresentativeAccount crep = new CustomerRepresentativeAccount("customer representative");
                try {
                    crep.lookUp(username);
                    // crep.account is the customer account
                    if(crep.account.getFirstName() == ""){
                        Toast.makeText(CustomerRepresentativeActivity.this, "User Not Found", Toast.LENGTH_LONG).show();
                    } else {
                        // go to account page. tee hee
                        // Toast.makeText(CustomerRepresentativeActivity.this, crep.account.getCableService().getName(), Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(CustomerRepresentativeActivity.this, crepAccountViewActivity.class);
                        intent.putExtra("username", crep.account.getUsername());
                        startActivity(intent);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                //Toast.makeText(CustomerRepresentativeActivity.this, parseUser.getUsername(), Toast.LENGTH_LONG).show();

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_customer_representative, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
