package com.swaglord.cse110telecomrevamp.AccountActivity;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.swaglord.cse110telecomrevamp.R;
import com.swaglord.cse110telecomrevamp.Utilities.PackageListAdapter;
import com.swaglord.cse110telecomrevamp.Utilities.ServiceListAdapter;

public class mrepEditPackageListActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mrep_edit_package_list);

        // Initialize the subclass of ParseQueryAdapter
        PackageListAdapter packageListAdapter = new PackageListAdapter(this);

        // Initialize ListView
        ListView packageList = (ListView) findViewById(R.id.editPackageListListView);
        packageList.setAdapter(packageListAdapter);

        findViewById(R.id.mrepAddPackageListButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =
                        new Intent(mrepEditPackageListActivity.this, mrepAddPackageListActivity.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.mrepRemovePackageListButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =
                        new Intent(mrepEditPackageListActivity.this, mrepRemovePackageListActivity.class);
                startActivity(intent);
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mrep_edit_package_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent marketingRepIntent = new Intent(this, MarketingRepresentativeActivity.class);
        marketingRepIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(marketingRepIntent);
    }
}
