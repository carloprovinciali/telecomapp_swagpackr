package com.swaglord.cse110telecomrevamp.Database;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SignUpCallback;
import com.swaglord.cse110telecomrevamp.Account.Account;
import com.swaglord.cse110telecomrevamp.Account.CustomerAccount;
import com.swaglord.cse110telecomrevamp.Login.DispatchActivity;
import com.swaglord.cse110telecomrevamp.R;
import com.swaglord.cse110telecomrevamp.Services.CableService;
import com.swaglord.cse110telecomrevamp.Services.InternetService;
import com.swaglord.cse110telecomrevamp.Services.PhoneService;
import com.swaglord.cse110telecomrevamp.Services.Service;
import com.swaglord.cse110telecomrevamp.Services.ServicePackage;

import java.util.List;

/**
 * Created by Ranger on 3/5/2015.
 */
public class DatabaseMediator {
    public static ParseUser parseUser;

    public static void login(String username, String password, final Context appContext){
        final ProgressDialog dialog = new ProgressDialog(appContext);
        dialog.setTitle("Please wait");
        dialog.setMessage("Please wait while you are logged in");
        dialog.show();

        ParseUser.logInInBackground(username, password, new LogInCallback() {
            @Override
            public void done(ParseUser parseUser, ParseException e) {
                dialog.dismiss();
                if(e != null){
                  Toast.makeText(appContext, e.getMessage(), Toast.LENGTH_LONG).show();
                }
                else{
                    Intent intent = new Intent(appContext, DispatchActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    String userType = getUserType(parseUser);
                    intent.putExtra("type", userType);
                    intent.putExtra("username", parseUser.getUsername());
                    appContext.startActivity(intent);
                }
            }
        });
    }

    //Get user type
    public static String getUserType(ParseUser user){
        return user.get("type").toString();
    }

//    public static CustomerAccount getUserSettingsAndServices(final CustomerAccount customer){
//        if(customer.getUsername() == null){
//            return null;
//        }
//        else{
//
//            //Get user services
//            ParseQuery<ParseObject> serviceQuery = ParseQuery.getQuery("UserServices");
//            serviceQuery.whereEqualTo("username", customer.getUsername());
//            serviceQuery.findInBackground(new FindCallback<ParseObject>() {
//                public void done(List<ParseObject> userServiceList, ParseException e) {
//                    if (e == null) {
//                        CableService cable = new CableService(userServiceList.get(0).getString("cable"),
//                                userServiceList.get(0).getDouble("cablePrice"));
//                        InternetService internet = new InternetService(userServiceList.get(0).getString("internet"),
//                                userServiceList.get(0).getDouble("internetPrice"));
//                        PhoneService phone = new PhoneService(userServiceList.get(0).getString("phone"),
//                                userServiceList.get(0).getDouble("phonePrice"));
//                        ServicePackage servicePackage = new ServicePackage(userServiceList.get(0).getString("package"),
//                                userServiceList.get(0).getDouble("packagePrice"), cable, internet, phone);
//                        customer.setCableService(cable);
//                        customer.setInternetService(internet);
//                        customer.setPhoneService(phone);
//                        customer.setServicePackage(new ServicePackage());
//
//                    } else {
//                    }
//                }
//            });
//
//            //Get user settings
//            ParseQuery<ParseObject> settingsQuery = ParseQuery.getQuery("UserSettings");
//            settingsQuery.whereEqualTo("username", customer.getUsername());
//            settingsQuery.findInBackground(new FindCallback<ParseObject>() {
//                @Override
//                public void done(List<ParseObject> userSettingsList, ParseException e) {
//                    customer.setBalanceThreshold(userSettingsList.get(0).getDouble("threshold"));
//                }
//            });
//
//        }
//        return customer;
//    }


    //Get user from database
    public static CustomerAccount getCustomer(String username) throws ParseException {
        CustomerAccount customer = new CustomerAccount(username, "", "", true);
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereEqualTo("username", username);
        List<ParseUser> user_list = query.find();
        if(user_list.size() == 0) {
            return customer;
        }
        ParseUser user = user_list.get(0);
        customer.setFirstName(user.get("firstName").toString());
        customer.setLastName(user.get("lastName").toString());
        customer.setRetail((boolean)user.get("isRetail"));
        customer.setType(user.get("type").toString());
        customer.setBill(user.getDouble("serviceBill"));

        ParseQuery<ParseObject> serviceQuery = ParseQuery.getQuery("UserServices");
        serviceQuery.whereEqualTo("username", username);
        List<ParseObject> userServiceList = serviceQuery.find();
        CableService cable = new CableService(userServiceList.get(0).getString("cable"),
                userServiceList.get(0).getDouble("cablePrice"));
        InternetService internet = new InternetService(userServiceList.get(0).getString("internet"),
                userServiceList.get(0).getDouble("internetPrice"));
        PhoneService phone = new PhoneService(userServiceList.get(0).getString("phone"),
                userServiceList.get(0).getDouble("phonePrice"));
        ServicePackage servicePackage = new ServicePackage(userServiceList.get(0).getString("package"),
                userServiceList.get(0).getDouble("packagePrice"), cable, internet, phone);
        customer.setCableService(cable);
        customer.setInternetService(internet);
        customer.setPhoneService(phone);
        customer.setServicePackage(servicePackage);


        return customer;
    }



    //Create new account by delegating to signupUser
    public static void signup(String username, String password,  String email,
                                 String firstName, String lastName,  String address,
                                 boolean isRetail, Context appContext) {

        signupUser(username, password, email, firstName, lastName, address, isRetail, appContext);
    }

    //Create new service and push it to the database
    public static boolean createService(Service service){

        //Handle package case
        if(service.getType().equals(Service.PACKAGE)){
            final ParseObject services = new ParseObject("Packages");
            ServicePackage servicePackage = (ServicePackage) service;
            services.put("name", servicePackage.getName());
            services.put("price", servicePackage.getPrice());
            services.put("type", servicePackage.getType());
            services.put("cable", servicePackage.getCableService());
            services.put("internet", servicePackage.getInternetService());
            services.put("phone", servicePackage.getPhoneService());
            try{
                services.save();
                return true;
            }
            catch (ParseException e) {
                return false;
            }
        }
        //Handle service case
        else{
            final ParseObject services = new ParseObject("Services");
            services.put("name", service.getName());
            services.put("price", service.getPrice());
            services.put("type", service.getType());
            try{
                services.save();
                return true;
            }
            catch (ParseException e){
                return false;
            }
        }
    }



    //Sign up the user
    private static void signupUser(final String username, final String password, String email, final String firstName,
                              final String lastName, String address,boolean isRetail, final Context appContext) {
        final ProgressDialog dialog = new ProgressDialog(appContext);
        dialog.setTitle("Please wait");
        dialog.setMessage("Please wait while you are signed up");
        dialog.show();

        final ParseUser user = new ParseUser();
        user.setUsername(username.toLowerCase());
        user.setPassword(password);
        user.setEmail(email);
        user.put("type", Account.CUSTOMER);
        user.put("serviceBill", 0);
        user.put("balanceThreshold", CustomerAccount.DEFAULT_THRESHOLD);
        user.put("firstName", firstName);
        user.put("lastName", lastName);
        user.put("address", address);
        user.put("isRetail", isRetail);

        user.signUpInBackground(new SignUpCallback() {
            @Override
            public void done(ParseException e) {
                dialog.dismiss();
                if(e != null){
                    Toast.makeText(appContext, e.getMessage(), Toast.LENGTH_LONG).show();
                }
                else{
                    initializeServices(username, appContext);
                    initializeSettings(username, appContext);
                    Toast.makeText(appContext, R.string.signup_success, Toast.LENGTH_LONG).show();
                    login(username, password, appContext);
                }
            }
        });
    }

    //Initialize user services called by signupUser
    private static void initializeServices(String username, Context appContext) {
        final ParseObject userServices = new ParseObject("UserServices");
        userServices.put("username", username);
        userServices.put("cable", CableService.NO_CABLE);
        userServices.put("cablePrice", 0);
        userServices.put("internet", InternetService.NO_INTERNET);
        userServices.put("internetPrice", 0);
        userServices.put("phone", PhoneService.NO_PHONE);
        userServices.put("phonePrice", 0);
        userServices.put("package", ServicePackage.NO_PACKAGE);
        userServices.put("packagePrice", 0);
        try{
            userServices.save();
        }
        catch (ParseException e){
            Toast.makeText(appContext, e.getMessage(), Toast.LENGTH_LONG);
        }
    }

    //Initialize user settings called by signupUser
    private static void initializeSettings(String username, Context appContext) {
        final ParseObject userSettings = new ParseObject("UserSettings");
        userSettings.put("username", username);
        userSettings.put("threshold", CustomerAccount.DEFAULT_THRESHOLD);
        try{
            userSettings.save();
        }
        catch (ParseException e){
            Toast.makeText(appContext, e.getMessage(), Toast.LENGTH_LONG);
        }
    }

    public static boolean removeService(Service serviceToRemove) throws ParseException {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Services");
        List<ParseObject> services_list = query.find();

        for(int i = 0; i < services_list.size(); i++) {
            ParseObject service = services_list.get(i);
            if(service.getString("type").equals(serviceToRemove.getType()) && service.getString("name").equals(serviceToRemove.getName())) {
                service.delete();
                return true;
            }
        }

        return false;
    }

    public static Service getService(String name, String type) throws ParseException {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Services");
        List<ParseObject> services_list = query.find();

        for(int i = 0; i < services_list.size(); i++) {
            ParseObject service = services_list.get(i);
            if(service.getString("name").equals(name) && service.getString("type").equals(type)) {
                if(type.equals(Service.CABLE)) {
                    return new CableService(service.getString("name"), service.getDouble("price"));
                }
                if(type.equals(Service.INTERNET)) {
                    return new InternetService(service.getString("name"), service.getDouble("price"));
                }
                if(type.equals(Service.PHONE)) {
                    return new PhoneService(service.getString("name"), service.getDouble("price"));
                }
            }
        }

        return null;
    }

    public static boolean addPackage(ServicePackage packageToAdd) throws ParseException {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Packages");
        List<ParseObject> packages_list = query.find();

        for(int i = 0; i < packages_list.size(); i++) {
            ParseObject servicePackage = packages_list.get(i);
            if(servicePackage.getString("name").equals(packageToAdd.getName())) {
                return false;
            }
        }

        ParseObject packageObject = new ParseObject("Packages");
        packageObject.put("name", packageToAdd.getName());
        packageObject.put("cable", packageToAdd.getCableService().getName());
        packageObject.put("internet", packageToAdd.getInternetService().getName());
        packageObject.put("phone", packageToAdd.getPhoneService().getName());
        packageObject.put("price", packageToAdd.getPrice());
        packageObject.saveEventually();

        return true;
    }

    public static boolean removePackage(String name) throws ParseException {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Packages");
        List<ParseObject> services_list = query.find();

        for(int i = 0; i < services_list.size(); i++) {
            ParseObject packageToRemove = services_list.get(i);
            if(packageToRemove.getString("name").equals(name)) {
                packageToRemove.delete();
                return true;
            }
        }

        return false;
    }

    public static ServicePackage getPackage(String name) throws ParseException {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Packages");
        List<ParseObject> packages_list = query.find();

        for(int i = 0; i < packages_list.size(); i++) {
            ParseObject packageToGet = packages_list.get(i);
            if(packageToGet.getString("name").equals(name)) {
                String packageName = packageToGet.getString("name");
                double packagePrice = packageToGet.getDouble("price");
                CableService cableService = new CableService(packageToGet.getString("cable"), 0);
                InternetService internetService = new InternetService(packageToGet.getString("internet"), 0);
                PhoneService phoneService = new PhoneService(packageToGet.getString("phone"), 0);

                return new ServicePackage(packageName, packagePrice, cableService, internetService, phoneService);
            }
        }

        return null;
    }

    public static boolean addToPackage(Service service, String package_name) throws ParseException {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Packages");
        List<ParseObject> packages_list = query.find();

        for(int i = 0; i < packages_list.size(); i++) {
            ParseObject packageToGet = packages_list.get(i);
            if(packageToGet.getString("name").equals(package_name)) {
                if(service.getType().equals(Service.CABLE)) {
                    packageToGet.put("cable", service.getName().toString());
                }
                if(service.getType().equals(Service.INTERNET)) {
                    packageToGet.put("internet", service.getName().toString());
                }
                if(service.getType().equals(Service.PHONE)) {
                    packageToGet.put("phone", service.getName().toString());
                }
                packageToGet.saveInBackground();
                return true;
            }
        }

        return false;
    }
    
    public static boolean removeFromPackage(Service service, String package_name) throws ParseException {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Packages");
        List<ParseObject> packages_list = query.find();

        for(int i = 0; i < packages_list.size(); i++) {
            ParseObject currentPackage = packages_list.get(i);
            if(currentPackage.getString("name").equals(package_name)) {
                if(service.getType().equals(Service.CABLE) && currentPackage.get("cable").equals(service.getName())) {
                    currentPackage.put("cable", CableService.NO_CABLE);
                }
                if(service.getType().equals(Service.INTERNET) && currentPackage.get("internet").equals(service.getName())) {
                    currentPackage.put("internet", InternetService.NO_INTERNET);
                }
                if(service.getType().equals(Service.PHONE) && currentPackage.get("phone").equals(service.getName())) {
                    currentPackage.put("phone", PhoneService.NO_PHONE);
                }
                currentPackage.saveInBackground();
                return true;
            }
        }

        return false;
    }

    public static void setNewService(String s, String username, Context appContext) throws ParseException{
        String svType;
        //int svPrice;

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Services");
        query.whereEqualTo("name", s);
        List<ParseObject> service_list = query.find();

        if(service_list.size() == 0){
            // not found
            Toast.makeText(appContext, "Invalid Service", Toast.LENGTH_LONG).show();

        } else {
            svType = service_list.get(0).get("type").toString();

            ParseQuery<ParseObject> query1 = ParseQuery.getQuery("UserServices");
            query1.whereEqualTo("username", username);
            List<ParseObject> user_list = query1.find();

            if(svType.equals(Service.CABLE)){
                user_list.get(0).put("cable", s);
                user_list.get(0).put("cablePrice", service_list.get(0).get("price"));
            } else if (svType.equals(Service.PHONE)){
                user_list.get(0).put("phone", s);
                user_list.get(0).put("phonePrice", service_list.get(0).get("price"));
            } else if(svType.equals(Service.INTERNET)) {    // internet
                user_list.get(0).put("internet", s);
                user_list.get(0).put("internetPrice", service_list.get(0).get("price"));
            }
            user_list.get(0).put("package", ServicePackage.NO_PACKAGE);
            user_list.get(0).put("packagePrice", 0);

            Toast.makeText(appContext, "Successfully updated service!", Toast.LENGTH_LONG).show();
            user_list.get(0).save();
        }
    }

    public static boolean changePrice(String package_name, double price) throws ParseException {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Packages");
        List<ParseObject> packages_list = query.find();

        for(int i = 0; i < packages_list.size(); i++) {
            ParseObject currentPackage = packages_list.get(i);
            if(currentPackage.getString("name").equals(package_name)) {
                currentPackage.put("price", price);
                currentPackage.saveInBackground();
                return true;
            }
        }

        return false;
    }
    public static void setNewPackage(String s, String username, Context appContext) throws ParseException{

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Packages");
        query.whereEqualTo("name", s);
        List<ParseObject> package_list = query.find();

        if(package_list.size() == 0){
            // not found
            Toast.makeText(appContext, "Invalid Package", Toast.LENGTH_LONG).show();

        } else {
            ParseQuery<ParseObject> query1 = ParseQuery.getQuery("UserServices");
            query1.whereEqualTo("username", username);
            List<ParseObject> user_list = query1.find();
            user_list.get(0).put("package", s);
            user_list.get(0).put("packagePrice", package_list.get(0).get("price"));

            // adding a package cancels other services
            user_list.get(0).put("internet", "No Internet Service");
            user_list.get(0).put("internetPrice", 0);
            user_list.get(0).put("phone", "No Phone Service");
            user_list.get(0).put("phonePrice", 0);
            user_list.get(0).put("cable", "No Cable Service");
            user_list.get(0).put("cablePrice", 0);

            Toast.makeText(appContext, "Successfully updated package!", Toast.LENGTH_LONG).show();
            user_list.get(0).save();
        }
    }

    public static void deleteValue(String s, String username, Context appContext) throws ParseException {
        String svType;
        ParseQuery<ParseObject> queryU = ParseQuery.getQuery("UserServices");
        queryU.whereEqualTo("username", username);
        List<ParseObject> user_list = queryU.find();

        // package or service?
        ParseQuery<ParseObject> queryP = ParseQuery.getQuery("Packages");
        queryP.whereEqualTo("name", s);
        List<ParseObject> package_list = queryP.find();
        if(package_list.size() == 0){
            // must be a service then
            ParseQuery<ParseObject> queryS = ParseQuery.getQuery("Services");
            queryS.whereEqualTo("name", s);
            List<ParseObject> service_list = queryS.find();

            if(service_list.size() == 0){
                Toast.makeText(appContext, "Invalid service or package name: " + s,
                        Toast.LENGTH_LONG).show();
            } else {
                // what is the type?
                svType = service_list.get(0).get("type").toString();
                if(svType.equals(Service.CABLE)){
                    user_list.get(0).put("cable", "Removed Cable Service");
                    user_list.get(0).put("cablePrice", 15);
                } else if (svType.equals(Service.PHONE)){
                    user_list.get(0).put("phone", "Removed Phone Service");
                    user_list.get(0).put("phonePrice", 15);
                } else if (svType.equals(Service.INTERNET)){    // internet
                    user_list.get(0).put("internet", "Removed Internet Service");
                    user_list.get(0).put("internetPrice", 15);
                }
                Toast.makeText(appContext, "Successfully deleted service: " + s ,
                        Toast.LENGTH_LONG).show();
            }

        } else {
            // delete the package
            user_list.get(0).put("package", "No Package");
            user_list.get(0).put("packagePrice", 0);
            Toast.makeText(appContext, "Successfully deleted package: " + s ,
                    Toast.LENGTH_LONG).show();
        }

        user_list.get(0).save();
    }

    public static void changeThreshold(String username, double threshold) throws ParseException{

        Context appContext;

        ParseQuery<ParseObject> query = ParseQuery.getQuery("UserSettings");
        query.whereEqualTo("username", username);
        List<ParseObject> threshold_list = query.find();

        threshold_list.get(0).put("threshold", threshold);
        threshold_list.get(0).save();
    }

    public static double getThreshold(String username) throws ParseException {

        double threshold;

        ParseQuery<ParseObject> query = ParseQuery.getQuery("UserSettings");
        query.whereEqualTo("username", username);
        List<ParseObject> threshold_list = query.find();

        threshold = threshold_list.get(0).getDouble("threshold");

        return threshold;
    }
}