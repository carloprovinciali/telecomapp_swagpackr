package com.swaglord.cse110telecomrevamp.Account;

import com.parse.Parse;
import com.parse.ParseException;
import com.swaglord.cse110telecomrevamp.Database.DatabaseMediator;
import com.parse.ParseUser;
import com.swaglord.cse110telecomrevamp.Services.*;
import com.swaglord.cse110telecomrevamp.Services.ServicePackage;

/**
 * Created by awong on 3/8/15.
 * commit, update, push. update
 */
public class CustomerRepresentativeAccount extends Account{
    private String customerUsername;
    public CustomerAccount account;
    //private ParseUser customer;

    public CustomerRepresentativeAccount(String username){
        this.Username = username;
        this.Type = CUSTOMER_REP;
    }


    // lookup customer account so we can change its stuff
    public void lookUp(String name) throws ParseException {
        //ParseUser customer = new ParseUser();     // empty ParseUser
        //customer.setUsername(name);
        account = DatabaseMediator.getCustomer(name);
    }

    // create new customer account
    public void createCustomerAccount(String username, String firstName, String lastName,
                                        boolean isRetail){
        account = new CustomerAccount(username, firstName, lastName, isRetail);
    }

    // getters
    public CableService getCustomerCableService() { return account.getCableService(); }
    public PhoneService getCustomerPhoneService(){ return account.getPhoneService(); }
    public InternetService getCustomerInternetService(){ return account.getInternetService(); }
    public ServicePackage getCustomerServicePackage() { return account.getServicePackage(); }

    // setters
    public void setCustomerCableService(CableService cs) { account.setCableService(cs); }
    public void setCustomerPhoneService(PhoneService ps){ account.setPhoneService(ps); }
    public void setCustomerInternetService(InternetService is) { account.setInternetService(is);}
    public void setCustomerServicePackage(ServicePackage sp){ account.setServicePackage(sp);}
}
