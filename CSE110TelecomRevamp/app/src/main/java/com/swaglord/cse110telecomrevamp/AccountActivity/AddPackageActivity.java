package com.swaglord.cse110telecomrevamp.AccountActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import com.parse.ParseException;
import com.swaglord.cse110telecomrevamp.Database.DatabaseMediator;
import com.swaglord.cse110telecomrevamp.R;
import com.swaglord.cse110telecomrevamp.Utilities.PackageListAdapter;

public class AddPackageActivity extends ActionBarActivity {

    private EditText packageToAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_package);

        Intent intent = getIntent();
        final String username = (String) intent.getSerializableExtra("username");

        // Initialize the subclass of ParseQueryAdapter
        PackageListAdapter packageListAdapter = new PackageListAdapter(this);

        // Initialize ListView
        ListView packageList = (ListView) findViewById(R.id.addPackageListView);
        packageList.setAdapter(packageListAdapter);

        packageToAdd = (EditText) findViewById(R.id.addPackagePackageName);

        // add package to the customer account
        findViewById(R.id.addPackageButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    // add service using DB mediator
                    DatabaseMediator.setNewPackage(packageToAdd.getText().toString(), username,
                            AddPackageActivity.this);

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_package, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
