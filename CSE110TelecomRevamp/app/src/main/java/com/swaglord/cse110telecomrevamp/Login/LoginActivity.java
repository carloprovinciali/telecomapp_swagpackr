package com.swaglord.cse110telecomrevamp.Login;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.swaglord.cse110telecomrevamp.Database.DatabaseMediator;
import com.swaglord.cse110telecomrevamp.R;
import com.swaglord.cse110telecomrevamp.Utilities.Utilities;

public class LoginActivity extends ActionBarActivity {
    private EditText Username;
    private EditText Password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Username = (EditText) findViewById(R.id.loginUsername);
        Password = (EditText) findViewById(R.id.loginPassword);

        findViewById(R.id.loginButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean invalidInformation = false;
                StringBuilder errorMessage =
                        new StringBuilder("Please enter the following fields: ");

                //Check for empty username field
                if (Utilities.isEmpty(Username)) {
                    invalidInformation = true;
                    errorMessage.append("username");
                }
                //Check for empty password field
                if (Utilities.isEmpty(Password)) {
                    if (invalidInformation) {
                        errorMessage.append(", ");
                    }
                    invalidInformation = true;
                    errorMessage.append("password");
                }
                errorMessage.append(".");

                if(invalidInformation){
                    Toast.makeText(LoginActivity.this, errorMessage.toString(), Toast.LENGTH_LONG).show();
                    return;
                }

                DatabaseMediator.login(Username.getText().toString().toLowerCase().trim(),
                        Password.getText().toString().trim(), LoginActivity.this);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
